#ifndef _IMAGE_H_
#define _IMAGE_H_ 1



class Image {
    public:

    Image();
    Image(int width, int height);
    Image(int width, int height, rgb background);

    bool set(int x, int y, const rgb &color);
    void gammaCorrect( float gamma);
    void writePPM(std::ostream &out);
    void readPPM(std::string file_name);

    private: 
    rgb **raster;
    int nx;
    int ny; 
};

Image::Image(){}

Image::Image(int width, int height){
    nx = width;
    ny = height;

    //allocate memory for raster
    raster = new rgb*[nx];
    for(int i = 0; i < nx ; i++){
        raster[i] = new rgb[ny];
    }
}

Image::Image(int width, int height, rgb background){
    nx = width;
    ny = height;
    //allocate memory for raster
    raster = new rgb*[nx];
    for(int i = 0; i < nx ; i++){
        raster[i] = new rgb[ny];
        for(int j = 0; j < ny ; j++)
        // assign background to each element;
            raster[i][j] = background;
    }   
}

bool Image::set(int x, int y, const rgb &color){
    //check for out of bounds errors
    if(0 > x || x > nx) return false;
    if(0 > y || y > ny) return false;

    raster[x][y] = color;
    return true;
}
void Image::gammaCorrect(float gamma){
    rgb temp;
    float power = 1.0 / gamma;
    for(int i = 0; i < nx; i++)
        for(int j = 0; j < ny; j++){
            temp = raster[i][j];
            raster[i][j] = rgb(pow(temp.r(), power), pow(temp.g(), power), pow(temp.b(), power));
            
            
        }
}

void Image::writePPM(std::ostream& out){
    //output header;
    out << "P3\n";
    out << nx << ' ' << ny << '\n';
    out << "255\n";
    int i, j;

    unsigned int ired, igreen, iblue;
    gammaCorrect(2.2);
    // output clamped [0, 255] values
    for(i = ny -1 ; i >= 0; i--)
        for(j=0; j < nx; j++){
            ired = (unsigned int)(256*raster[j][i].r());
            igreen= (unsigned int)(256*raster[j][i].g());
            iblue = (unsigned int)(256*raster[j][i].b());
        
            if (ired > 255) ired = 255;
            if (igreen > 255) igreen = 255;
            if (iblue > 255) iblue = 255;
            out << ired<< ' ' << igreen << ' ' << iblue << '\n';
        }
}

void Image::readPPM(std::string file_name){
    std::ifstream in;
    in.open(file_name.c_str());
    if(!in.is_open())
    {
        std::cerr << " ERROR - could not open file\n";
        exit(-1);

    }
}
#endif

