#ifndef KDTREE
#define KDTREE

struct photon {
  vec3 power;
  vec3 position;
  vec3 direction;
  vec3 surface_normal;
};

struct node {

  int dim;
  photon p;
  node *left;
  node *right;
};

node* create_node(photon p){
  node * n = (node *)malloc(sizeof(node));
  n->dim = 0;
  n->p = p;
  
  n->left = NULL;
  n->right = NULL;

  return n;
  
}

node * add_node(node *root, node *new_node){

  if(root == NULL){
    root = new_node;
    return root;
  }
  
  int current_dim = root->dim;
  int new_dim = (current_dim + 1) % 3;
  if(root->p.position[root->dim] >= new_node->p.position[root->dim]){
    new_node->dim = new_dim;
    root->left = add_node(root->left, new_node);
  }
  else {
    new_node->dim = new_dim;
    root->right = add_node(root->right, new_node);
  }

  return root;
}
void nearest_neighbours(node *root, vec3 point, float d, int accumulated_photons, int n){
  if(root == NULL)
    return;

  if( n >= accumulated_photons)
    return;
  
  float dist = (root->p.position[0]-point[0])*(root->p.position[0]-point[0])
    + (root->p.position[1]-point[1])*(root->p.position[1]-point[1])
    + (root->p.position[2]-point[2])*(root->p.position[2]-point[2]);
  
  //std::cout << root->p.position[0] << "," << root->p.position[1] << "," << root->p.position[2] << std::endl;
  //std::cout << point[0] << "," << point[1] << "," << point[2] << std::endl;
  if(dist > 0)
    dist = sqrt(dist);
  //std::cout << dist << "\n";
  if(dist <= d){ // add to the photons inside the sphere.
    // NOTE: Let us just start with this and check it in matlab.
    std::cout << root->p.position[0] << "," << root->p.position[1] << "," << root->p.position[2] << std::endl;
  }
  
  if(root->p.position[root->dim] < point[root->dim]){
    
    nearest_neighbours(root->right, point, d);
    nearest_neighbours(root->left, point, d);
  }
  else {
    nearest_neighbours(root->left, point, d);
    nearest_neighbours(root->right, point,d);
  }

  return;

}
void print_tree(node *root, std::ofstream &file){
  
  if(root == NULL)
    return;
  
  
  

  print_tree(root->left, file);
  print_tree(root->right, file);
  file << root->p.position[0] << "," << root->p.position[1] << "," << root->p.position[2] << std::endl;
  // std::cout << root->p.position[0] << " " << root->p.position[1] << " " << root->p.position[2] << "dim : " << root->dim << std::endl;
  
}  
  
#endif
