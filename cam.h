#ifndef CAMERAH
#define CAMERAH

#define M_PI 3.14159265359

class camera {
public:
  camera(vec3 look_from, vec3 look_at, vec3 vup, float vfov, float aspect){

    vec3 u,v,w;

    float theta = vfov * M_PI / 180.0;
    float half_height = tan(theta/2.0);
    float half_width = aspect * half_height;

    origin = look_from;
    w = unit_vector(look_from - look_at);
    u = unit_vector(cross(vup,w));
    v = cross(w,u);
    
    //lower_left_corner = vec3(-half_width, -half_height, -1.0);
    lower_left_corner = origin - half_width*u - half_height*v - w;
    horizontal = 2.0 * half_width * u;
    vertical = 2.0 * half_height * v;
    //    horizontal = vec3(2.0*half_width, 0.0, 0.0);
    //vertical = vec3(0.0, half_height*2.0, 0.0);
    //origin = vec3(0.0, 0.0, 0.0);
  }

  ray get_ray(float s, float t){
    return ray(origin, lower_left_corner + s*horizontal + t*vertical - origin);
  }

  vec3 lower_left_corner;
  vec3 horizontal;
  vec3 vertical;
  vec3 origin;
};

#endif
