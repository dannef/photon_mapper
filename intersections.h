#ifndef INTERSECTIONSH
#define INTERSECTIONSH

bool hit_sphere(const vec3& center, float radius, const ray& r){
  vec3 oc = r.origin() - center;
  float a = dot(r.direction(), r.direction());
  float b = 2.0* dot(oc, r.direction());
  float c = dot(oc, oc) - radius*radius;
  float disc = b*b - 4*a*c;

  return (disc > 0);
}
bool xy_plane_intersect(const vec3& n, const vec3 &p0, const ray& r){
  
}
#endif
