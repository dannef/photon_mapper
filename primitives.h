#ifndef PRIMITIVESH
#define PRIMITIVESH

#include "hitable.h"



class flip_normals : public hitable {
 public:
  flip_normals(hitable *p) : ptr(p) {}
  virtual bool hit(const ray &r, float tmin, float tmax, hit_record &rec) const {
    if(ptr->hit(r, tmin, tmax, rec)){
      rec.normal = -rec.normal;
      return true;
    }
    else
      return false;
  }

  hitable *ptr;
};

class triangle: public hitable {
public:
  triangle(){}
  triangle(vec3 _v1, vec3 _v2, vec3 _v3, material *m) : v1(_v1), v2(_v2), v3(_v3), mat_ptr(m) {};

  virtual bool hit(const ray &r, float tmin, float tmax, hit_record &rec) const;

  vec3 v1;
  vec3 v2;
  vec3 v3;

  material *mat_ptr;
};
bool triangle::hit(const ray& r, float tmin, float tmax, hit_record &rec) const {
  float a = v1.x() - v2.x();
  float b = v1.y() - v2.y();
  float c = v1.z() - v2.z();

  float d = v1.x() - v3.x();
  float e = v1.y() - v3.y();
  float f = v1.z() - v3.z();

  float g = r.direction().x();
  float h = r.direction().y();
  float i = r.direction().z();

  float j = v1.x() - r.origin().x();
  float k = v1.y() - r.origin().y();
  float l = v1.z() - r.origin().z();

  float ei_minus_hf = e*i - h*f;
  float gf_minus_di = g*f - d*i;
  float dh_minus_eg = d*h - e*g;
  
  float M = a*(ei_minus_hf) + b*(gf_minus_di) + c*(dh_minus_eg);

  float beta = (j*(ei_minus_hf) + k*(gf_minus_di) + l*(dh_minus_eg)) / M;
  if(beta <= 0.0f || beta >= 1.0f) return false;
  float gamma = (i*(a*k - j*b) + h*(j*c - a*l) + g*(b*l - k*c)) / M;
  if(gamma <= 0.0f || beta + gamma >= 1.0f) return false;
  
  float t = -(f*(a*k - j*b) + e*(j*c - a*l) + d*(b*l - k*c))/M;


  
  if(t >= tmin && t <= tmax){
    rec.t = t;
    rec.p = r.at(rec.t);
    rec.normal = unit_vector(cross(v1, v2));
    rec.mat_ptr = mat_ptr;
    return true;
  }      
  return false;
}
class sphere: public hitable {
public:
  sphere(){}
  sphere(vec3 cen, float r, material *m) : center(cen), radius(r), mat_ptr(m) {};
  virtual bool hit(const ray& r, float tmin, float tmax, hit_record &rec) const;

  vec3 center;
  float radius;
  material *mat_ptr;
};

bool sphere::hit(const ray& r, float t_min, float t_max, hit_record& rec) const {
    vec3 oc = r.origin() - center;
    float a = dot(r.direction(), r.direction());
    float b = dot(oc, r.direction());
    float c = dot(oc, oc) - radius*radius;
    float discriminant = b*b - a*c;
    if (discriminant > 0) {
        float temp = (-b - sqrt(discriminant))/a;
        if (temp < t_max && temp > t_min) {
            rec.t = temp;
            rec.p = r.at(rec.t);
            rec.normal = (rec.p - center) / radius;
	    rec.mat_ptr = mat_ptr;
            return true;
        }
        temp = (-b + sqrt(discriminant)) / a;
        if (temp < t_max && temp > t_min) {
            rec.t = temp;
            rec.p = r.at(rec.t);
            rec.normal = (rec.p - center) / radius;
	    rec.mat_ptr = mat_ptr;
            return true;
        }
    }
    return false;
}
// add material here later::

class xz_rect : public hitable {
public:
  xz_rect() {}
  xz_rect(float _x0, float _x1, float _z0, float _z1, float _k, material *m, int n) :
    x0(_x0), x1(_x1), z0(_z0), z1(_z1), k(_k), mat_ptr(m), normal_mult(n) {};

  virtual bool hit(const ray& r, float tmin, float tmax, hit_record &rec) const;
  
  float x0;
  float x1;
  float z0;
  float z1;
  float  k;
  material *mat_ptr;
  int normal_mult; // used to invert normal.
};

bool xz_rect::hit(const ray& r, float tmin, float tmax, hit_record &rec) const {

  float t = (k - r.origin().y()) / r.direction().y();

  if(t < tmin || t > tmax)
    return false;


  float x = r.origin().x() + t * r.direction().x();
  float z = r.origin().z() + t * r.direction().z();

  if( x < x0 || x > x1 || z < z0 || z > z1)
    return false;
  
  
  //rec.u = (x-x0) / (x1 - x0);
  //rec.v = (y-y0) / (y1 - y0);

  rec.t = t;
  rec.p = r.at(t);
  rec.normal = vec3(0,1 * normal_mult,0);
  rec.mat_ptr = mat_ptr;
  return true;
}

class xy_rect : public hitable {
public:
  xy_rect() {}
  xy_rect(float _x0, float _x1, float _y0, float _y1, float _k, material *m, int n) :
    x0(_x0), x1(_x1), y0(_y0), y1(_y1), k(_k), mat_ptr(m), normal_mult(n) {};

  virtual bool hit(const ray& r, float tmin, float tmax, hit_record &rec) const;
  
  float x0;
  float x1;
  float y0;
  float y1;
  float  k;
  material *mat_ptr;
  int normal_mult;
};

bool xy_rect::hit(const ray& r, float tmin, float tmax, hit_record &rec) const {

  float t = (k - r.origin().z()) / r.direction().z();

  if(t < tmin || t > tmax)
    return false;


  float x = r.origin().x() + t * r.direction().x();
  float y = r.origin().y() + t * r.direction().y();

  if( x < x0 || x > x1 || y < y0 || y > y1)
    return false;
  
  
  //rec.u = (x-x0) / (x1 - x0);
  //rec.v = (y-y0) / (y1 - y0);

  rec.t = t;
  rec.p = r.at(t);
  rec.normal = vec3(0,0,1 * normal_mult);
  rec.mat_ptr = mat_ptr;
  return true;
}
class yz_rect : public hitable {
public:
  yz_rect() {}
  yz_rect(float _y0, float _y1, float _z0, float _z1, float _k, material *m, int n) :
    y0(_y0), y1(_y1), z0(_z0), z1(_z1), k(_k), mat_ptr(m), normal_mult(n) {};

  virtual bool hit(const ray& r, float tmin, float tmax, hit_record &rec) const;
  
  float y0;
  float y1;
  float z0;
  float z1;
  float  k;
  material *mat_ptr;
  int normal_mult;
};

bool yz_rect::hit(const ray& r, float tmin, float tmax, hit_record &rec) const {

  float t = (k - r.origin().x()) / r.direction().x();

  if(t < tmin || t > tmax)
    return false;


  float y = r.origin().y() + t * r.direction().y();
  float z = r.origin().z() + t * r.direction().z();
  //std::cout << "x: " << x << ", y: " << y <<  "x0,x1,y0,y1: " << x0 << ", " << x1 << ", " << y0 << ", " << y1  << "\n";
  /*
  if(x < x0){
    std::cout << "failed: x < x0 -> " << x << "< " << x0 << "\n";
    return false;
  }
  
  if(x > x1){
    std::cout << "failed: x > x1 -> " << x << "> " << x1 << "\n";
    return false;
  }
  
  if(y < y0){
    std::cout << "failed: y < y0 -> " << y << "< " << y0 << "\n";
    return false;
  }
  
  if(y > y1){
    std::cout << "failed: y > y1 -> " << y << "> " << y1 << "\n";
    return false;
  }
  */
  if( y < y0 || y > y1 || z < z0 || z > z1)
    return false;
  
  
  //rec.u = (x-x0) / (x1 - x0);
  //rec.v = (y-y0) / (y1 - y0);

  rec.t = t;
  rec.p = r.at(t);
  rec.normal = vec3(1 * normal_mult,0,0);
  rec.mat_ptr = mat_ptr;
  return true;
}


#endif
