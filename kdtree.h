#ifndef KDTREEH
#define KDTREEH

#include <algorithm>



void swap(vec3* a, vec3* b) 
{ 
    vec3 t = *a; 
    *a = *b; 
    *b = t; 
} 

int partition(std::vector<vec3> &photons, int low, int high, int dim){
  float pivot = photons[high][dim];
  int i = (low - 1);

  for(int j = low ; j <= high - 1; j++){

    if ( photons[j][dim] <= pivot){
      i++;
      swap(&photons[i], &photons[j]);
    }
  }

  swap(&photons[i+1], &photons[high]);
  return (i+1);
}

void quick_sort(std::vector<vec3> &photons, int low, int high, int dim){

  if(low < high){
    int piv = partition(photons, low, high, dim);

    quick_sort(photons, low, piv - 1, dim);
    quick_sort(photons, piv +1, high, dim);
  }
}


struct node {

  int k;

  node *left;
  node *right;
};
int test_find_median(const int index, const int start, const int end){
  int median = 1;
  while((4*median) <= (end-start + 1))
    median += median;
  if((3*median) <= (end-start+1)) {
    median += median;
    median += start-1;
  }
  else median = end - median + 1;

  return median;
}

node * new_node(int k){
  node * n = (node *)malloc(sizeof(node));
  n->k = k;
  n->left = NULL;
  n->right = NULL;
  return(n);
}
node * insert(node * root_node, int val){
  if(root_node == NULL) return new_node(val);

  if(val > root_node->k)
    root_node->right = insert(root_node->right, val);
  else if(val < root_node->k)
    root_node->left = insert(root_node->left, val);

  return root_node;
}

void print_tree(node * n){
  if(n != NULL){
    print_tree(n->left);
    std::cout << n->k << "\n";
    print_tree(n->right);
  }
}

void free_tree(node *n){

  if(n = NULL)
    return;
  free_tree(n->left);
  free_tree(n->right);
  free(n);
}
/*
float max_vec3_list(vector<vec3> &photons, int dim){

  float max = 0;
  for(int i = 0; i < photons.size(); i++){
    if(photons[i][dim] > max)
      max = photons[i][dim];
  }

  return max;
}

float min_vec3_list(vector<vec3> &photons, int dim){

  float min = infinity;
  for(int i = 0; i < photons.size(); i++){
    if(photons[i][dim] < min)
      min = photons[i][dim];
  }

  return min;
}

void print_tree(node * n, int d){
  if(n == NULL)
    return;
  std::cout << "At depth " << d << ": ";
  std::cout << n->median_vector[0] << " " << n->median_vector[1] << " " << n->median_vector[2] << "\n";

  print_tree(n->left, d+1);
  print_tree(n->right, d+1);

  return;
}
*/
/*
node * balance(vector<int> &photons, node * n){


  if(photons.size() <= 1){
    return new_node(photons[0]);


    if( 
  float x_dist = abs(max_vec3_list(photons, 0) - min_vec3_list(photons,0));
  float y_dist = abs(max_vec3_list(photons, 0) - min_vec3_list(photons,0));
  float z_dist = abs(max_vec3_list(photons, 0) - min_vec3_list(photons,0));
  
  vector<vec3> lower;
  lower.resize(photons.size()/2);

  vector<vec3> upper;
  upper.resize(photons.size()/2);

  int lower_index = 0;
  int upper_index = 0;
  
  int max_dim = 0;
  if(x_dist >= y_dist & x_dist >= z_dist)
    max_dim = 0;
  if(y_dist >= x_dist & y_dist >= z_dist)
    max_dim = 1;
  if(z_dist >= y_dist & z_dist >= x_dist)
    max_dim = 2;       
  
  quick_sort(photons, 0, photons.size()-1, max_dim);
  
  float median = photons[photons.size()/2][max_dim];
  
  for(int i = 0; i < photons.size(); i++){
    if(median <= photons[i][max_dim])
      lower[lower_index++] = photons[i];
    else
      upper[upper_index++] = photons[i];
  }
  
  
  std::cout << "new" << "\n";
  n->median_vector = photons[photons.size()/2];
  n->left = balance(lower, n->left);
  n->right = balance(upper, n->right);

  return n;

}
*/
#endif
