#ifndef KDTREE
#define KDTREE

struct photon {
  vec3 power;
  vec3 position;
  vec3 direction;
  vec3 surface_normal;
};

struct node {

  int dim;
  photon p;
  node *left;
  node *right;
};

node* create_node(photon *p){
  node * n = (node *)malloc(sizeof(node));
  n->dim = 0;
  n->p = p;
  
  n->left = NULL;
  n->right = NULL;

  return n;
  
}

node * add_node(node *root, node *new_node){

  if(root == NULL){
    root = new_node;
    return root;
  }
  
  int current_dim = root->dim;
  int new_dim = (current_dim + 1) % 2;
  if(root->vector[root->dim] >= new_node->vector[root->dim]){
    new_node->dim = new_dim;
    root->left = add_node(root->left, new_node);
  }
  else {
    new_node->dim = new_dim;
    root->right = add_node(root->right, new_node);
  }

  return root;
  

}
void nearest_neighbours(node *root, node *point, float d){
  if(root == NULL)
    return;

  float dist = (root->vector[0]-point->vector[0])*(root->vector[0]-point->vector[0])
    + (root->vector[1]-point->vector[1])*(root->vector[1]-point->vector[1]);

  if(dist > 0)
    dist = sqrt(dist);
  std::cout << dist << "\n";
  if(dist <= d){
    std::cout << root->vector[0] << " " << root->vector[1] << " " << root->vector[2] << std::endl;
  }
  
  if(root->vector[root->dim] <= point->vector[root->dim]){
    
    nearest_neighbours(root->right, point, d);
  }
  else {

    nearest_neighbours(root->left, point, d);
  }

  return;

}
void print_tree(node *root){
  
  if(root == NULL)
    return;
  
  std::cout << root->vector[0] << " " << root->vector[1] << " " << root->vector[2] << "dim : " << root->dim << std::endl;

  print_tree(root->left);
  print_tree(root->right);
  
}  
  
#endif
