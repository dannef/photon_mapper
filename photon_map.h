#ifndef PHOTON_MAPH
#define PHOTON_MAPH

#define M_PI 3.14159265358979323846264338327950288
/* Implementation according to Realistic Image Synthesis by Henrik Wann Jensen */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

// 28 byte photon
struct photon{
  float pos[3]; // Photon position
  short plane; // Splitting plane for kd-tree
  unsigned char theta, phi; // incoming direction (spherical)
  float power[3]; // photon power, uncompressed
};

struct nearest_photons {
  int max;
  int found;
  int got_heap;
  float pos[3];
  float dist2;
  const photon **index;
};



class photon_map {
public:
  photon_map(int max_phot);
  ~photon_map();


  void store(const float power[3], const float pos[3], const float dir[3]); // photon power, pos, direction
  void scale_photon_power(const float scale); // 1 / number of emitted photons
  void balance();

  void irradiance_estimate(float irrad[3], // returned irradiance
			   const float pos[3], // surface position
			   const float normal[3], // surface normal at pos
			   const float max_dist, // max dist to look for photons
			   const int nphotons) const; // number of photons to use
  
  void locate_photons(nearest_photons *const np, // used to locate the photons
		      const int index) const;
  void photon_dir(float *dir,
		  const photon *p) const;

  photon *photons;
  int stored_photons;
  int half_stored_photons;
  int max_photons;
  int prev_scale;

  float costheta[256];
  float sintheta[256];
  float cosphi[256];
  float sinphi[256];

  float bbox_min[3];
  float bbox_max[3];
private:
  void balance_segment(photon **pbal,
		       photon **porg,
		       const int index,
		       const int start,
		       const int end);

  void median_split(photon **p,
		    const int start,
		    const int end,
		    const int median,
		    const int axis);

};

photon_map::photon_map(const int max_phot){
  stored_photons = 0;
  prev_scale = 1;
  max_photons = max_phot;

  photons = (photon*)malloc(sizeof(photon) * (max_photons +1));

  if(photons == NULL)
    std::cout << "Could not initialize photon map" << std::endl;

  bbox_min[0] = bbox_min[1] = bbox_min[2] = 1e8f;
  bbox_max[0] = bbox_max[1] = bbox_max[2] = -1e8f;


  // Initialize direction conversion tables;

  for(int i = 0 ; i < 256; i++){
    double angle = double(i) * (1.0 / 256.0) * M_PI;
    costheta[i] = cos(angle);
    sintheta[i] = sin(angle);
    cosphi[i] = cos(2.0 * angle);
    sinphi[i] = sin(2.0 * angle);
  }
}

photon_map::~photon_map(){
  free(photons);
}

void photon_map::photon_dir(float *dir, const photon *p) const {
  dir[0] = sintheta[p->theta]*cosphi[p->phi];
  dir[1] = sintheta[p->theta]*sinphi[p->phi];
  dir[2] = costheta[p->theta];

}
void photon_map::store(const float power[3],
		       const float pos[3],
		       const float dir[3]){
  if(stored_photons >= max_photons)
    return;

  stored_photons++;
  photon *const node = &photons[stored_photons]; // Get allocated memory of a photon struct.

  for(int i = 0; i < 3; i++){
    node->pos[i] = pos[i];

    if(node->pos[i] < bbox_min[i])
      bbox_min[i] = node->pos[i];
    if(node->pos[i] > bbox_max[i])
      bbox_max[i] = node->pos[i];

    node->power[i] = power[i];
  }
  int theta = int(acos(dir[2]) * (256.0 / M_PI));
  if(theta > 255)
    node->theta = 255;
  else
    node->theta = (unsigned char)theta;

  int phi = int(atan2(dir[1], dir[0])*(256.0 / (2.0*M_PI)));
  if(phi > 255)
    node->phi = 255;
  else if(phi < 0)
    node->phi = (unsigned char)(phi+256);
  else
    node->phi = (unsigned char)phi;
}
#define swap(ph, a, b) { photon *ph2= ph[a]; ph[a] = ph[b]; ph[b] = ph2; }

void photon_map::median_split(photon **p,
			      const int start,
			      const int end,
			      const int median,
			      const int axis){
  int left = start;
  int right = end;
 
  while(right > left){
    const float v = p[right]->pos[axis];

    int i = left-1;
    int j = right;
    for(;;){
      
      while(p[++i]->pos[axis] < v); // Hitta f�rsta index p� element som �r st�rre �n v      
      while(p[--j]->pos[axis] > v && j > left); // Hitta index fr�n h�ger p� f�rsta elem som �r mindre �n v      
      if(i >= j)
	break;
      swap(p, i, j);       
    }
  
    swap(p,i,right);
    if(i >= median)
      right = i-1;
    if(i <= median)
      left = i + 1;
      }
}


void photon_map::balance(){
  // Generate a temporary place to store pointers to each of the photons
  if(stored_photons > 1){
    photon **pa1 = (photon**)malloc(sizeof(photon*)*(stored_photons+1));
    photon **pa2 = (photon**)malloc(sizeof(photon*)*(stored_photons+1));


    // Create list of pointers so we can easily manipulate them
    for(int i = 0; i <= stored_photons; i++){
      pa2[i] = &photons[i];
    }
    std::cout << stored_photons << std::endl;
    balance_segment(pa1, pa2, 1, 1, stored_photons);
   
    free(pa2);

    // Reorganize the balanced kd-tree, that is, make a kd-heap now.
    int d, j = 1, foo = 1;
    photon foo_photon = photons[j];
    for(int i = 1; i <= stored_photons; i++){
      d = pa1[j] - photons; // Subtract addresses/pointer values, this gives an index
      pa1[j] = NULL; // NULL jth element of pa1;
      if( d != foo )
	photons[j] = photons[d];
      else {
	photons[j] = foo_photon;

	if(i < stored_photons){
	  for(;foo <= stored_photons; foo++)
	    if(pa1[foo] != NULL)
	      break;
	  foo_photon = photons[foo];
	  j = foo;
	}
	continue;
      }
      j = d;
    }
    free(pa1);
  }
  half_stored_photons = (int)stored_photons/2 - 1 ;
}

    
void photon_map::balance_segment(photon **pbal, photon **porg,
		     const int index,
		     const int start,
		     const int end){
  // Find dimension to split at
  // Compute new desired median
  
  int median = 1;
  while((4*median) <= (end - start + 1))
    median += median;

  if((3*median) <= (end - start + 1)) {
    median += median;
    median += start - 1;
  }
  else
    median = end - median + 1;

  int axis = 2;
  if((bbox_max[0] - bbox_min[0]) > (bbox_max[1] - bbox_min[1])  &&
     (bbox_max[0] - bbox_min[0]) > (bbox_max[2] - bbox_min[2]))
    axis = 0;
  else if((bbox_max[1] - bbox_min[1]) > (bbox_max[2] - bbox_min[2]))
    axis = 1;
 
 
  median_split(porg, start, end, median, axis);

  pbal[index] = porg[median];
  pbal[index]->plane = axis;
  /*
   for(int i = 1 ; i <= stored_photons; i++){
    std::cout << "photon " << i << ": ";
    std::cout << porg[i]->pos[0] << ", ";
    std::cout << porg[i]->pos[1] << ", ";
    std::cout << porg[i]->pos[2] << "\n";
  }
  std::cout << "\n"; */
   //recursively balance left and right block;
  if(median > start){
    //balance left segment;
    if(start < median-1){
      const float tmp = bbox_max[axis];
      bbox_max[axis] = pbal[index]->pos[axis];
      balance_segment(pbal, porg, 2*index, start, median-1);
      bbox_max[axis] = tmp;
    }
    else
      pbal[2*index] = porg[start];
  }
  if(median < end) {
    if(median + 1 < end){
      const float tmp = bbox_min[axis];
      bbox_min[axis] = pbal[index]->pos[axis];
      balance_segment(pbal, porg, 2*index+1, median +1, end);
      bbox_min[axis] = tmp;
    }
    else
      pbal[2*index+1] = porg[end];
  }   

}
#endif
/*
void photon_map::irradiance_estimate(vec3 irrad,
				     const vec3 pos,
				     const vec3 normal,
				     const float max_dist,
				     const int nphotons) const {
  
  irrad[0] = irrad[1] = irrad[2] = 0.0;
  nearest_photons np;
  np.dist2 = (float*)alloca(sizeof(float)*(nphotons+1));
  np.index = (const photon**)alloca(sizeof(photon*)*(nphotons+1));

  np.pos[0] = pos[0];
  np.pos[1] = pos[1];
  
}

void photon_map::store(const float power[3],
		       const float pos[3],
		       const float dir[3]){
  if(stored_photons >= max_photons)
    return;

  stored_photons++;
  photon *const node = &photons[stored_photons]; // Get allocated memory of a photon struct.

  for(int i = 0; i < 3; i++){
    node->pos[i] = pos[i];

    if(node->pos[i] < bbox_min[i])
      bbox_min[i] = node->pos[i];
    if(node->pos[i] > bbox_max[i])
      bbox_max[i] = node->pos[i];

    node->power[i] = power[i];
  }
  int theta = int(acos(dir[2]) * (256.0 / M_PI));
  if(theta > 255)
    node->theta = 255;
  else
    node->theta = (unsigned char)theta;

  int phi = int(atan2(dir[1], dir[0])*(256.0 / (2.0*M_PI)));
  if(phi > 255)
    node->phi = 255;
  else if(phi < 0)
    node->phi = (unsigned char)(phi+256);
  else
    node->phi = (unsigned char)phi;
}

void photon_map::scale_photon_power(const float scale){
  for(int i= prev_scale; i <=stored_photons; i++){
    photons[i].power[0] *= scale;
    photons[i].power[1] *= scale;
    photons[i].power[2] *= scale;
  }
  prev_scale = stored_photons+1;
}

void photon_map::balance(){
  if(stored_photons > 1){

    // Generate a temporary place to store pointers to each of the photons
    photon **pa1 = (photon**)malloc(sizeof(photon*)*(stored_photons+1));
    photon **pa2 = (photon**)malloc(sizeof(photon*)*(stored_photons+1));

    for(int i = 0; i <= stored_photons; i++)
      pa2[i] = &photons[i]; // assign address to each pointer of each photon, such that we can easily move them around

    balance_segment(pa1, pa2, 1, 1, stored_photons);
    free(pa2);

    // Reorganize the balanced kd-tree, that is, make a kd-heap now.
    int d, j = 1, foo = 1;
    photon foo_photon = photons[j];
    for(int i = 1; i <= stored_photons; i++){
      d = pa1[j] - photons; // Subtract addresses/pointer values, this gives an index
      pa1[j] = NULL; // NULL jth element of pa1;
      if( d != foo )
	photons[j] = photons[d];
      else {
	photons[j] = foo_photon;

	if(i < stored_photons){
	  for(;foo <= stored_photons; foo++)
	    if(pa1[foo] != NULL)
	      break;
	  foo_photon = photons[foo];
	  j = foo;
	}
	continue;
      }
      j = d;
    }
    free(pa1);
  }
  half_stored_photons = stored_photons/2 - 1 ;
}

#define swap(ph, a, b) { photon *ph2= ph[a]; ph[a] = ph[b]; ph[b] = ph2; }

void photon_map::median_split(photon **p,
			      const int start,
			      const int end,
			      const int median,
			      const int axis){
  int left = start;
  int right = end;

  while(right > left){
    const float v = p[right]->pos[axis];
    int i = left-1;
    int j = right;
    for(;;){
      while(p[i++]->pos[axis] < v);
      while(p[--j]->pos[axis] > v && j > left);

      if(i >= j)
	break;
      swap(p, i, j);
    }

    swap(p,i,right);
    if(i >= median)
      right = i-1;
    if(i <= median)
      left = i + 1;
  }
}

void photon_map::balance_segment(photon **pbal,
				 photon **porg,
				 const int index,
				 const int start,
				 const int end){
  // Compute new desired median
  
  int median = 1;
  while((4*median) <= (end - start + 1))
    median += median;

  if((3*median) <= (end - start + 1)) {
    median += median;
    median += start - 1;
  }
  else
    median = end - median + 1;


  // Find axis to split along
  int axis = 2 ;
  if((bbox_max[0] - bbox_min[0]) > (bbox_max[1] - bbox_min[1]) &&
     (bbox_max[0] - bbox_min[0]) > (bbox_max[2] - bbox_min[2]))
    axis = 0;
  else if((bbox_max[1] - bbox_min[1]) > (bbox_max[2] - bbox_min[2]))
    axis = 1;
  
  // partition photon block around median
  median_split(porg, start, end, median, axis);

  pbal[index] = porg[median];
  pbal[index]->plane = axis;

  //recursively balance left and right block;
  if(median > start){
    //balance left segment;
    if(start < median-1){
      const float tmp = bbox_max[axis];
      bbox_max[axis] = pbal[index]->pos[axis];
      balance_segment(pbal, porg, 2*index, start, median-1);
      bbox_max[axis] = tmp;
    }
    else
      pbal[2*index] = porg[start];
  }
  if(median < end) {
    if(median + 1 < end){
      const float tmp = bbox_min[axis];
      bbox_min[axis] = pbal[index]->pos[axis];
      balance_segment(pbal, porg, 2*index+1, median +1, end);
      bbox_min[axis] = tmp;
    }
    else
      pbal[2*index+1] = porg[end];
  }
}
*/


    
