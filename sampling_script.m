X = importdata('ppt.csv');
%%
scatter3(X(:,1), X(:,2), X(:,3))
xlabel('x')
ylabel('y')
zlabel('z')
hold on
%%
scatter3(555.0, 294, 203, 'filled')
%%
Y = importdata('points.csv')
scatter3(Y(:,1), Y(:,2), Y(:,3),'filled')

%%
k = linspace(-3,3, 300);

fun = @(x, var, w) w*((1/ (2 * pi * var)) * exp(-(x.*x) / ( 2*var)))


plot(k, fun(k, v_normalized(1,1), v_normalized(2,1)))
hold on
plot(k, fun(k, v_normalized(1,1), v(2,1)))
hold off
%%
plot(k, fun(k, v_normalized(1,2), v_normalized(2,2)))
hold on
plot(k, fun(k, v_normalized(1,2), v(2,2)))