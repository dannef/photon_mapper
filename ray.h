#ifndef RAYH
#define RAYH

class ray {
public:
  ray() {}
  ray(const vec3& org, const vec3& dir) { d = dir; o = org;}

  vec3 origin() const {return o;}
  vec3 direction() const {return d;}
  vec3 at(float t) const {return o + d*t;}

  vec3 o;
  vec3 d;
};

#endif
