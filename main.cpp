#include <cmath>
#include <string>
#include <fstream>
#include <iostream>


#include "utilities.h"
#include "rgb.h"
#include "image.h"
#include "vec3.h"
#include "ray.h"
#include "material.h"
#include "hitablelist.h"
#include "primitives.h"
#include "cam.h"


#include "float.h"
#include <chrono>
#include <vector>
#include <fstream>

#include <algorithm>
#include <stdlib.h>
#include "kdtrees.h"
using namespace std::chrono;
using namespace std;

// Globals
node *root;

//hitable *world;

//vector <photon> photon_map;

void store_photon(vec3 position, vec3 power, vec3 direction, vec3 s_n){
  photon p;

  p.power = power;
  p.position = position;
  p.direction = direction;
  p.surface_normal = s_n;

  //photon_map.push_back(position);
  node *n = create_node(p);
  root = add_node(root, n);
}

void trace_photon(const ray &r, const hitable *world, vec3 power){

  hit_record rec;
  ray _r;
  bool russian = true;
  float p = 0.5;
  
  if(world->hit(r, 0.001, infinity, rec)){
    //power /= p;
    //power *= dot(-r.direction, rec.normal);
    //std::cout <<  rec.p.x() << ", " << rec.p.y() << ", " << rec.p.z() << "\n";
    store_photon(rec.p, power, r.direction(), rec.normal);
  }
}
void light_tracing(int p, const hitable *world){

  // divide all photons at the end by the total number of photons. We do not know how many
  // we are doing initially because we are using russian roulette i think.
  int emitted_photons = 0;
  vec3 power(1.0, 1.0, 1.0);
  vec3 pos(555/2, 555/2, 555/2);
  float x,y,z;
  while( emitted_photons < p){
    do{
      x = 2 * random_double() - 1;
      y = 2 * random_double() - 1;
      z = 2 * random_double() - 1;
    }while(x*x + y*y + z*z > 1);
    emitted_photons++;
    vec3 d(x,y,z);

    ray r;
    r.o = pos;
    r.d = d;
     

    //photon_map.push_back();
    trace_photon(r, world, power);
  }
  // Scale power of stored photons by n;
}

vec3 color(const ray &r, const hitable *world, int depth){

  hit_record rec;
  ray _r = r;


  vec3 L = vec3(0.0, 0.0, 0.0);
  vec3 path_throughput = vec3(1.0, 1.0, 1.0);


  if(!world->hit(_r, 0.001, infinity, rec)){
    vec3 unit_direction = unit_vector(_r.direction());
    float t = 0.5*(unit_direction.y() + 1.0);
    return (1.0-t)*vec3(1.0, 1.0, 1.0) + t* vec3(0.0, 0.2, 1.0);
  }

  for(int bounces = 0; bounces < 1; bounces++){

    if(world->hit(_r, 0.001, infinity, rec)){

      ray scattered;
      vec3 attenuation;
      rec.mat_ptr->scatter(_r, rec, attenuation, scattered);
      L = L + path_throughput * attenuation;

      _r = scattered;
    }
    else
      return L;
  }

  return L;
  /*
  if(world->hit(r, 0.001, infinity, rec)){
    //return 0.5 * vec3(rec.normal.x()+1, rec.normal.y()+1, rec.normal.z()+1);
    // vec3 target = rec.p + rec.normal + random_in_unit_sphere();
    //return 0.5 * color(ray(rec.p, target - rec.p), world, 0);

    
    ray scattered;
    vec3 attenuation;
    return vec3(1.0, 0.0, 0.0);
    /* 
    if(depth < 50 && rec.mat_ptr->scatter(r, rec, attenuation, scattered)){
      return attenuation * color(scattered, world, depth+1);
    }
    else
    return vec3(0,0,0);*/
     
    /*
  vec3 unit_direction = unit_vector(r.direction());
  float t = 0.5*(unit_direction.y() + 1.0);
  return (1.0-t)*vec3(1.0, 1.0, 1.0) + t* vec3(0.5, 0.7, 1.0);  */
}
hitable_list *cornell_box(){
  hitable **list = new hitable*[6];
  int i = 0;

  material *bottom_color = new lambertian(vec3(0.75,0.75,0.75));
  material *left_color = new lambertian(vec3(0.75f, 0.25f,0.25f));
  material *right_color = new lambertian(vec3(0.25, 0.25, 0.75));
  material *back_color = new lambertian(vec3(0.15,0.15, 0.55));
  material *white = new lambertian(vec3(0.75,0.75, 0.75));


  list[i++] = new yz_rect(0,555,0,555,555, left_color, 1) ;
  list[i++] = new yz_rect(0,555,0, 555, 0, right_color, 1);
  list[i++] = new xz_rect(0,555,0, 555, 555, white, -1);
  list[i++] = new xz_rect(0,555,0, 555, 0, white, -1);
  list[i++] = new xy_rect(0,555,0, 555, 555, back_color, 1);
  //list[i++] = new sphere(vec3(555/2, 555/2, 555/2), 100, new lambertian(vec3(0.8, 0.3, 0.3)));

  return new hitable_list(list, i);
  
}
int main(){

  int nx = 400;
  int ny = 400;
  int ns = 10;
  
  const int objects = 2;
  
  hitable *list[objects];

  //list[0] = new sphere(vec3(0, 0, -1), 0.5, new lambertian(vec3(0.8, 0.3, 0.3)));
  //list[1] = new sphere(vec3(0, -100.5, -1), 100, new lambertian(vec3(0.75, 0.75, 0.75)));
  //list[2] = new sphere(vec3(-102.0, 0.0,-1.0), 100, new lambertian(vec3(0.75f, 0.25f, 0.25f)));
  //list[3] = new sphere(vec3(102.0, 0.0, -1), 100, new lambertian(vec3(0.25f, 0.25f, 0.75f)));
  //list[4] = new sphere(vec3(0, 0.0, -102.0f), 100, new lambertian(vec3(0.75f, 0.75f, 0.75f)));
  //list[5] = new sphere(vec3(0, 102.5, -1), 100, new lambertian(vec3(0.75, 0.75, 0.75)));

  /*
    d_list[2] = new sphere(vec3(0,-100.5f,-1), 100.0f, vec3(0.75, 0.75, 0.75), vec3(0), new diffuse(), DIFFUSE); //bottom
    d_list[3] = new sphere(vec3(-102.0f, 0, -1), 100.0f, vec3(0.75f, 0.25f, 0.25f), vec3(0), new diffuse(), DIFFUSE); // left
    d_list[4] = new sphere(vec3(102.0f, 0.0f,-1), 100.0f, vec3(0.25f, 0.25f, 0.75f), vec3(0), new diffuse(), DIFFUSE); //right
    d_list[5] = new sphere(vec3(0.0f, 0.0f,-102.0f), 100.0f, vec3(0.75f, 0.75f, 0.75f), vec3(0), new diffuse(), DIFFUSE); //back
    d_list[6] = new sphere(vec3(0.0f, 102.0f,-1.0), 100.0f, vec3(0.75, 0.75, 0.75), vec3(0), new diffuse(), DIFFUSE); //top
    d_list[7] = new sphere(vec3(0.0f, 0.0f,105.0f), 100.0f, vec3(0.75, 0.75, 0.75), vec3(0), new diffuse(), DIFFUSE); //front    
  */
  //list[0] = new xy_rect(-1.5,1,-1,1,-2, new lambertian(vec3(0.8, 0.3,0.3)));
  //list[1] = new xz_rect(-1,1,-1,1,-0.5);
  //list[0] = new yz_rect(-0.5,0.5,-1.5,-1.0, 0.4);

  vec3 v1(-1.0f,-1.0f,-1.5);
  vec3 v2(1.0, -1, -1.5);
  vec3 v3(0.0f, 1.0, -1.5);
  //list[1] = new triangle(v1, v2, v3, new lambertian(vec3(0.1, 0.3,0.3)));
 
  vector<vec3> image;

  image.resize(nx*ny);
  int index = 0;
  
  auto start = high_resolution_clock::now();
  
  //hitable_list *world = new hitable_list(list, objects);
  hitable_list *world = cornell_box();
  //camera cam(vec3(-2, 2, 1), vec3(0, 0, -1), vec3(0, 1, 0), 90, float(nx)/float(ny));
  // camera cam(vec3(0.0, 0.0, 1.0), vec3(0, 0, -1), vec3(0, 1, 0), 90, float(nx)/float(ny));
  light_tracing(1000, world);
  ofstream img2;
  img2.open("ppt.csv");
  print_tree(root, img2);
  //root = create_node(photon_map[0]);
  for(int i = 1; i < 10; i++){
    //std::cout <<  photon_map[i].x() << ", " << photon_map[i].y() << ", " << photon_map[i].z() << "\n";
    //node *x = create_node(photon_map[i]);
    //add_node(root, x);
  }
  float x,y,z;
  bool not_hit = true;
  hit_record rec;
  ray _r;
    
  while(not_hit){
    do{
      x = 2 * random_double() - 1;
      y = 2 * random_double() - 1;
      z = 2 * random_double() - 1;
    }while(x*x + y*y + z*z > 1);

    vec3 d(x,y,z);
    ray r;
    r.o = vec3(555/2, 555/2, 555/2);
    r.d = d;
     
    
    if(world->hit(r, 0.001, infinity, rec)){
      not_hit = false;
      //std::cout << "found hit at: "
      //	<<  rec.p.x() << ", " << rec.p.y() << ", " << rec.p.z() << "\n";
    }
  }
  vec3 s(rec.p.x(), rec.p.y(), rec.p.z());


  nearest_neighbours(root, s, 100);
  
  vec3 a(0, 86.519, 323.888);
  vec3 b(555, 294.741, 203.656);

  float a_ = (a[0] - b[0]) * (a[0] - b[0]);
  float b_ = (a[1] - b[1]) * (a[1] - b[1]);
  float c_ = (a[2] - b[2]) * (a[2] - b[2]);
  /*
  
  camera cam(vec3(278, 278, -800), vec3(278, 278, 0), vec3(0, 1, 0), 40, float(nx)/float(ny));

  #pragma omp parallel for
  for(int j = ny-1; j >= 0; j--){
    for(int i = 0; i < nx; i++){
      vec3 col(0,0,0);
      for(int s = 0; s < ns; s++){
	float u = float(i + random_double()) / float(nx);
	float v = float(j + random_double()) / float(ny);
	
	ray r = cam.get_ray(u,v);
	
	col += color(r, world, 0);
      }

      col /= float(ns);

      image[j*nx + i] = col;
      
    
  
    }
  }
  auto stop = high_resolution_clock::now();

  auto duration = duration_cast<microseconds>(stop-start);
  std::cout << duration.count() / 1000000.0 << " seconds elapsed" << std::endl;

  std::cout << "printing to out.ppm..."<< std::endl;
  ofstream img;
  img.open("out.ppm");
  img << "P3\n" << nx << " " << ny << "\n255\n";
  for (int i = nx*ny; i >= 0; i--){
    int ir = int(255.99*sqrt(image[i][0]));
    int ig = int(255.99*sqrt(image[i][1]));
    int ib = int(255.99*sqrt(image[i][2]));
    img << ir << " " << ig << " " << ib << "\n";
  }
  img.close();
  std::cout << "complete!" << "\n";
  */
}
