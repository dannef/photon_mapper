#include<stdlib.h>
#include<stdio.h>
#include<iostream>
#include<math.h>
#include<random>
struct node {

  int dim;
  float vector[3];
  node *left;
  node *right;
};

  
node* create_node(int dim, float *vec){
  node * n = (node *)malloc(sizeof(node));
  n->dim = 0;
  n->vector[0] = vec[0];
  n->vector[1] = vec[1];
  n->vector[2] = vec[2];

  n->left = NULL;
  n->right = NULL;

  return n;
  
}

node * add_node(node *root, node *new_node){

  if(root == NULL){
    root = new_node;
    return root;
  }
  
  int current_dim = root->dim;
  int new_dim = (current_dim + 1) % 2;
  if(root->vector[root->dim] >= new_node->vector[root->dim]){
    new_node->dim = new_dim;
    root->left = add_node(root->left, new_node);
  }
  else {
    new_node->dim = new_dim;
    root->right = add_node(root->right, new_node);
  }

  return root;
  

}
void nearest_neighbours(node *root, node *point, float d){
  if(root == NULL)
    return;

  float dist = (root->vector[0]-point->vector[0])*(root->vector[0]-point->vector[0])
    + (root->vector[1]-point->vector[1])*(root->vector[1]-point->vector[1]);

  if(dist > 0)
    dist = sqrt(dist);
  std::cout << dist << "\n";
  if(dist <= d){
    std::cout << root->vector[0] << " " << root->vector[1] << " " << root->vector[2] << std::endl;
  }
  
  if(root->vector[root->dim] <= point->vector[root->dim]){
    
    nearest_neighbours(root->right, point, d);
  }
  else {

    nearest_neighbours(root->left, point, d);
  }

  return;

}
void print_tree(node *root){
  
  if(root == NULL)
    return;
  
  std::cout << root->vector[0] << " " << root->vector[1] << " " << root->vector[2] << "dim : " << root->dim << std::endl;

  print_tree(root->left);
  print_tree(root->right);
  
}  

int main(){
  std::default_random_engine seed;
  std::uniform_real_distribution<float>random_float(0.0, 1.0);
  
  float v[3] = {1,2,3};
  float v1[3] = {2,3,4};
  float v2[3] = {5,6,7};
  float v3[3] = {7,1,3};

  float point[3] = {6,2,1};
  float pepe[3] = {random_float(seed)*10.0f, random_float(seed)*10.0f, random_float(seed)*10.0f};


  node * N = create_node(0, v);
    for (int i = 0; i < 5; i++){
    float veni[3] = {random_float(seed)*10.0f, random_float(seed)*10.0f, random_float(seed)*10.0f};
    
    node *temp = create_node(0, veni);
    std::cout << temp->vector[0] << " " << temp->vector[1] << " " << temp->vector[2] << std::endl;
    add_node(N, temp);
    
    }
    
  node *p = create_node(0, point);
  /*
  node *N2 = create_node(0, v1);
  node *N3 = create_node(0, v2);
  node *N4 = create_node(0, v3);
  node *N5 = create_node(0, v3);
	

  

  std::cout << N->vector[0] << " " << N->vector[1] << " " << N->vector[2] << std::endl;

  add_node(N,N2);
  add_node(N,N3);
  add_node(N,N4);
  add_node(N,N5);
    */
  print_tree(N);
  //std::cout << "\n";
  nearest_neighbours(N, p, 3.5f);




  return 0;
}
  
